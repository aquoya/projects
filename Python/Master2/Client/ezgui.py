import Tkinter as tk

"""EzWindow class creates a Tkinter frame and has abstract methods that can add tools to the frame.
run() - Starts display
size() - Changes the size of the display
label() - Displays text at a given row/column on the display.
button() - Displays button at a given row/column on the display.
entry() - Displays an empty box that the user can type in which then saves the prompt to a list and returns the reference to that item.
secret() - Similar to entry but instead displays '*' character instead of the character itself.
getentry() - Retrieves the item at the index of the reference list.
clearentry() - Clears the entry/secret tool on the GUI.
release_entry() - Deletes the item at the index of the reference list.
defaultfont() - Sets the default fonttype and fontsize.
quit() - Closes the window. (***NEEDS FIXING***)
"""

# Tool that helps represent specific GUI windows for the EzSchedules program.
# Class-scoped variables:
    # frame: this gui
    # references: list of references to every entry.
class EzWindow:

    default_font_type = "Times New Roman"
    default_font_size = 24

    # Constructor: Creates the window and initializes it with a title.
    def __init__(self, title):
        self.frame = tk.Tk()
        self.frame.title = title
        self.references = []
        self.refnum = 0
        #self.pagelist = PageList(5)

    def run(self):
        self.frame.mainloop()

    def size(self, width=500, height=500):
        self.width = width
        self.height = height
        self.frame.geometry(str(width) + "x" + str(height))

    def label(self, row, col, text, bgc="white", fgc="black", fonttype="Times New Roman", fontsize=12):
        lbl = tk.Label(self.frame, text=text, font=(fonttype, fontsize))
        lbl.grid(column=col,row=row)

    def button(self, row, col, command, bgc="white", fgc="black", text="No Label", width=15, height=2):
        btn = tk.Button(self.frame, text=text, command=command, bg=bgc, fg=fgc, width=width, height=height)
        btn.grid(column=col, row=row)

    def entry(self, col, row):
        txt = tk.Entry(self.frame, width = 10)
        txt.grid(column=col, row=row)
        self.references.append(txt)
        self.refnum += 1
        return self.refnum - 1

    def secret(self, col, row):
        txt = tk.Entry(self.frame, show="*", width = 10)
        txt.grid(column=col, row=row)
        self.references.append(txt)
        self.refnum += 1
        return self.refnum - 1

    def getentry(self, ref):
        txt = self.references[ref]
        return txt.get()

    def clearentry(self, ref):
        if self.references[ref]:
            self.references[ref].delete(0, tk.END)

    def release_entry(self, ref):
        self.references[ref] = ""

    def defaultfont(self, fonttype="Times New Roman", fontsize=24):
        Window.default_font_type = fonttype
        Window.default_font_size = fontsize

    def quit(self):
        self.frame.quit()
