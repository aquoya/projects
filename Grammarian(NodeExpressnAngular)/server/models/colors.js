const mongoose = require('mongoose');

const ColorsSchema = new mongoose.Schema( {
    guessBackground : {type : String, default : "#ffffff"},
    textBackground : {type : String, default : "#000f0f"},
    wordBackground : {type : String, default : "00ff00"}
});

const Colors = mongoose.model( 'Colors', ColorsSchema);

module.exports = Colors;