/*
 * CS 351 Problem 4 (Craps)
 * 
 * Estimated probability of winning over 100,000 replications:
 * 0.49031 only .49 is consitent with 100,000 replications
 * 
 */

import java.util.Random;

public class Problem4 {
	// craps
	Random random;

	public Problem4() {
		random = new Random();
	}

	public static void main(String[] args) {
		Problem4 p = new Problem4();
		int numSims = 100000;
		int total = 0;
		for (int i = 0; i < numSims; i++) {
			if (p.craps()) {
				total += 1;
			}
		}
		System.out.println(1.0 * total / numSims);

	}

	private boolean craps() {
		// Simple method to simulate a game of craps
		int d1 = rollDie();
		int d2 = rollDie();
		int dice = d1 + d2;
		if (dice == 7 || dice == 11) {
			return true;
		} else if (dice == 2 || dice == 3 || dice == 12) {
			return false;
		} else {
			int point = d1 + d2;
			d1 = rollDie();
			d2 = rollDie();
			while (d1 + d2 != point && d1 + d2 != 7) {
				d1 = rollDie();
				d2 = rollDie();
			}
			if (d1 + d2 == point) {
				return true;
			}
			return false;
		}
	}


	private int rollDie() {
		return 1 + random.nextInt(6);
	}

}
