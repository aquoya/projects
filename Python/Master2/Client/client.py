import socket as s
import threading as th

import ezgui
import pages as PAGES
#Julia
"""Client Class is the where the User mainly operates off of. Processes all of the commands coming from the server and controls communications between all connected users."""

class Client:
    sock = s.socket(s.AF_INET, s.SOCK_STREAM) # IPV4 Protocol with TCP connection
    port_number = 10044

    def __init__(self, address):
        lp = PAGES.LoginPage()

        self.sock.connect(address, Client.port_number) # Connect to the given ip address through configured port 10044

        recv_thread = th.Thread(target=self.sc_transfer)
        recv_thread.daemon = True
        recv_thread.start()

        lp = PAGES.LoginPage(self.sock)
        lp.window.run()
        self.action("close_connection")

    def sc_transfer(self):
        while(True):
            #try:
            data = self.sock.recv(1024) # Receive up to 1024 bytes of data.
            self.action(data)
            #except:
                #print("Error")

    def action(self, data):
        if data == "close_connection":
            self.sock.send("close_connection")
            self.sock.close()
            exit()
        elif data == "successful_login":
            print "@TODO Handle the case where username and password is correct. (Continue to the next page in the client.)"
        elif data == "unsuccessful_login":
            print "@TODO: Handle the case where username or password is incorrect."
        elif data == "account_creation_username_taken":
            print "@TODO: Handle the case where username is already taken."

print "Enter 'debug' to connect to Virtual Machine, where server is always available but may not be updated."
print "Enter 'main' to connect to Main Server, where server is not always available but may be getting continuously restarted/updated."
print "Otherwise enter ip address to connect to a third-party server."
add_prompt = raw_input()
if add_prompt == "debug":
    address = "138.49.184.109"
elif add_prompt == "main":
    address = "96.40.242.16"
else:
    address = add_prompt
print address
client = Client(address)
