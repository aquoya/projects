var express = require('express');
var router = express.Router();
var fs = require('fs');

const mongoose = require('mongoose');
let User = require("../models/user");
let Blog = require("../models/user");

router.post("/:userId/create", (req, res, next) => {
    if(req.headers.csrf_token !== req.session.csrf_token) {
        res.status(401).json({msg: "Error"});
    }

    Blog.create({ownerId:req.params.userId, text: req.body, published: false},
    (err, savedData) => {
        if(err) {
            res.status(403).json("ERROR");
        }
        res.status(200).json(savedDate);
    });

});

router.put("/:userId/:blogId", (req, res, next) => {
    if(req.headers.csrf_token !== req.session.csrf_token) {
        res.status(401).json({msg: "Error"});
    }
    Blog.findByIdAndUpdate(req.params.blogId, 
        {text: req.body, ownerId: req.params.userId, published: req.query.published},(err, data) => {
        if(err) {
            res.status(404).json("Error");
        }
        res.json(data);
        
    });
});

router.put("/:userId/:blogId/updatepublished", (req, res, next) => {
    if(req.headers.csrf_token !== req.session.csrf_token) {
        res.status(401).json({msg: "Error"});
    }
    let b = req.query.published;
    Blog.findByIdAndUpdate(req.params.blogId, {published : b}, (err, data) => {
        if(err) {
            res.status(404).json("Error");
        }
        res.json(data);
    });
});

router.get("/blogs", (req, res, next) => {
    if(req.headers.csrf_token !== req.session.csrf_token) {
        res.status(401).json({msg: "Error"});
    }
    let pubFilter = req.params.published;
    let ownerFilter = req.params.ownerId;

    Blogs.find({published : pubFilter, ownerId: ownerFilter}, (err, blogs) => {
        if(err) {
            res.status(404).json("Error");
        }
        let blogs = blogs.filter(b => {
            if(b.ownerId !== req.session.user._id && !published) {
                return false;
            } else {
                return true;
            }
        });
        res.json(blogs);
    });
});

router.get("/blogs/:blogId", (req, res, next)=>{
    if(req.headers.csrf_token !== req.session.csrf_token) {
        res.status(401).json({msg: "Error"});
    }
    Blog.findById(req.params.blogId, (err, data) => {
        if(err) {
            res.status(404).json("Error");
        } else if(data.ownerId !== req.session.user._id && !published) {
            res.status(401).send("unavailable");
        }
        res.json(data);
    });
});

router.delete("/:userId/:blogId", (req, res, next) => {
    if(req.headers.csrf_token !== req.session.csrf_token) {
        res.status(401).json({msg: "Error"});
    }
    Blog.findByIdAndDelete(req.params.blogId, (err, data) => {
        if(err) {
            res.status(404).json("Error");
        }
        res.json(data);
    });
});
