const mongoose = require('mongoose');

const DefaultsSchema = new mongoose.Schema( {
    font : { type : mongoose.Schema.ObjectId},
    level : { type : mongoose.Schema.ObjectId},
    colors : { type : mongoose.Schema.ObjectId}
});

const Defaults = mongoose.model( 'Defaults', DefaultsSchema);

module.exports = Defaults;