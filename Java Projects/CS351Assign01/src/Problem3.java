/*
 * CS 351 Program 3 (Intervals)
 * Aquoya Faust
 * 
 * 
 * (0, 1): 1.0
 * (0, 10): .72597
 * (0, 100): .67211
 * (0, 1000): .66886
 * 
 * General Rule: As we increase the size of the interval we will see the average slowly regress to 2/3. or .66666666666666666666667
 * This makes sense because if we take any interval in order for it to be twice the size of the other interval it would have to be in the outer 1/3 of either side.
 * That way what remains is at least 2/3 which is double 1/3. 
 */

import java.util.Random;

public class Problem3 {
	
	int interval;
	int numSims = 100000;
	public Problem3(int interval) {
		this.interval = interval;
	}

	public static void main(String[] args) {
		Problem3 prob = new Problem3(1);
		Problem3 prob1 = new Problem3(10);
		Problem3 prob2 = new Problem3(100);
		Problem3 prob3 = new Problem3(1000);
		
		prob.runSim();
		prob1.runSim();
		prob2.runSim();
		prob3.runSim();
		

	}
	
	private void runSim() {
		Random rand = new Random();
		int total= 0;
		for (int i = 0; i < numSims; i++) {
			int subIntervalA = rand.nextInt(interval + 1);
			//double subIntervalA = rand.nextDouble() * interval;
			int subIntervalB = interval - subIntervalA;
			//double subIntervalB = interval - subIntervalA;
			if (subIntervalA > 2 * subIntervalB || subIntervalB > 2 * subIntervalA) {
				total += 1;
			}
		}
		System.out.println((1.0 * total)/numSims);
	}

}
