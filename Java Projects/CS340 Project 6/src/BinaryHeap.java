public class BinaryHeap {
	// implements a binary heap of where the heap rule is the value in the parent
	// node is less than
	// or equal to the values in the child nodes
	private class Item implements Comparable {
		private int node;
		private int distance; // the priority

		private Item(int n, int d) {
			node = n;
			distance = d;
		}

		public int compareTo(Object z) {
			return distance - ((Item) z).distance;

		}
	}

	private Item items[];
	private int locations[]; // Locations is current position of the node
	private int size;

	public BinaryHeap(int s) {
		items = new Item[s + 1];
		locations = new int[s];
		// initially no nodes have been inserted
		for (int i = 0; i < locations.length; i++) {
			locations[i] = -1;
			items[i] = new Item(0, 0);
		}
		size = 0;
	}

	public void removeMin() {
		// PRE: getSize() != 0
		// removes the highest priority item in the queue
		int parent;
		int child;
		locations[items[1].node] = -1;
		Item x = items[size];
		size--;
		child = 2;
		while (child <= size) {
			parent = child / 2;
			if (child < size && items[child + 1].compareTo(items[child]) < 0) child++;
			if (x.distance < items[child].distance) break;
			items[parent] = items[child];
			locations[items[child].node] = locations[items[parent].node];
			child = 2 * child;
		}
		items[child / 2] = x;
		locations[x.node] = child / 2;
	}

	public int getMinNode() {
		// PRE: getSize() != 0
		// returns the highest priority node
		return items[1].node;
	}

	public int getMinDistance() {
		// PRE: getSize() != 0
		// returns the distance of highest priority node
		return items[1].distance;

	}

	public boolean full() {
		// return true if the heap is full otherwise return false
		return size == items.length - 1;
	}

	public void insert(int n, int d) {
		// PRE !full() and !inserted(n))
		Item item = new Item(n, d);
		if (locations[n] != -1 && item.compareTo(items[locations[n]]) < 0) {
			decreaseKey(n, d);
			return;
		} else if (locations[n] != -1)
			return;
		int parent;
		int child;
		size++;
		child = size;
		parent = child / 2;
		items[0].node = n;
		items[0].distance = d;
		while (items[parent].distance > d) {
			items[child] = items[parent];
			locations[items[parent].node] = locations[items[child].node];
			child = parent;
			parent = child / 2;
		}
		items[child] = item;
		locations[n] = child;

	}

	public void decreaseKey(int n, int d) {
		// PRE inserted(n) and d < the distance associated with n
		// replace the current distance associated with n with d
		Item nn = new Item(n, d);
		int parent;
		int child;
		child = locations[n];
		parent = child / 2;
		items[0].node = n;
		items[0].distance = d;
		while (items[parent].compareTo(nn) > 0) {
			items[child] = items[parent];
			locations[items[parent].node] = locations[items[child].node];
			child = parent;
			parent = child / 2;
		}
		items[child] = nn;
		locations[n] = child;
	}

	public int getSize() {
		// return the number of values (priority , tree) pairs in the heap
		return size;

	}

	public boolean inserted(int n) {
		return locations[n] != -1;
	}

}