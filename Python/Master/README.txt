Instructions to set up Server:
  - The server will be running constantly until May 25th where I (Travis Zuleger) will terminate the instance on EUCALYPTUS unless the instance is needed longer.
This means the client should automatically connect to the server with no issues, however if there are, the next steps can be taken or contacting me (zuleger5656@uwlax.edu).
  - The server itself should be running on a EUCALYPTUS cloud under the IP address: 138.49.184.109 on PORT 10044.
The folder Server should contain a .pem file that is a key to access the EUCALYPTUS server curiosity is struck. The command to access the VM would be:
        ssh -i ezschedules.pem ubuntu@138.49.184.109
After ssh'ing into the instance, there should be a file called 'server.py', just run the file with the command
        python server.py
and no issues should come from this.
  - In order to run the server from own IP address, the user would have to port forward on their router from port 10044 to that IP address
where the server would be hosted on.


Instructions to set up Client:
  - The client should automatically run so long the Server is running. If the server being set up is a different IP address from the above address (138.49.184.109) then the code
needs to be altered in the Client.py file from connecting to 138.49.184.109 to the IP address that needs to be connected to.

Navigation on the UI of the Client:
  - The first page that pops up will be the User's main LOGIN page, this page will also have the developer buttons and tools that were created during development.
      - Sign in: Only works if the username and password match up with the database's Usernames and Passwords.
      - Sign up: Only works if the username is available and password and retyped password both match.
      - Testing: Automatically goes to the CasualPage of the EzSchedules program.
      - Test Connection: Pings the server where the server pings back to verify the connection is good.
      - Submit Code: Submits a file that lies in the ./Client/submissions directory to the server.
  - The second page that pops up will be the User's CasualPage, this page really only has one function and thats to go to view the schedules set up for that user.
      - Create Schedule: Pulls up new page that allows the User to edit days and add events. (Currently does not support removing events)
      - Confirm Schedule: Does nothing, but is meant to send a designated schedule to a designated friend of the user's.
      - View Schedule: Does nothing, but is meant to open up the next Event to come up for the User.
  - The third page that pops up will be the User's CalendarPage, this page is built to look like the current month and allows the user to manipulate their events.
  - The fourth page that pops up will be the User's DailyViewPage, this page shows all of the events for that day and allows the user to add an event (if the day is current or future)
  - The fifth page that pops up will be the User's AddEventPage, this page just gives the user the option to add an Event at a specific start/end time.

Known bugs that can cause problems with testing:
  - MOST KNOWN BUG: Sometimes when the server and client attempt to communicate, they desync which causes the entire program to be null unless CTRL+C is pressed in Command Line (Linux/Mac)
where the program could be restarted (recommended) or the user can continue to try navigation but pages that pop up afterward may not be 100% accurate like they should be.
      - This happens almost 10% of the time. It isn't often but when it does, it's best to just close all windows and restart the program.
