import java.util.concurrent.TimeUnit;

import desmoj.core.dist.BoolDistBernoulli;
import desmoj.core.dist.ContDistExponential;
import desmoj.core.dist.ContDistNormal;
import desmoj.core.simulator.*;
import desmoj.core.statistic.Accumulate;
import desmoj.core.statistic.Count;
import desmoj.core.statistic.Tally;
/**
 * 
 * @author Aquoya Faust
 * 
 * Statistics
 * Minimum, Maximum, Average Response Times: 10.98	114.108	  39.766 Minutes respectively
 * Maximum Parts in System:	13
 * Average and Maximum Length of Process Queue:	2.378  and  11  Parts
 * Average and Maximum Length of Inspection Queue:	1.08  and  6 Parts
 * Utilization rate of Processor:	97.79%
 * Utilization rate of Inspector:	97.6%
 */
public class MachineProcessModel extends Model{
	protected Generator generator;
	protected ContDistExponential interArrivalTimes;
	protected ContDistExponential processingTimes;
	protected ContDistNormal inspectionTimes;
	protected ContDistExponential refiningTimes;
	protected BoolDistBernoulli needsRefining;
	
	protected ProcessQueue<Processor> idleProcessorQueue;
	protected ProcessQueue<Inspector> idleInspectorQueue;
	
	protected ProcessQueue<Part> processingQueue;
	protected ProcessQueue<Part> inspectionQueue;
	
	protected Accumulate pUtil;
	protected Accumulate iUtil;

	protected Count totalParts;
	// minimum maximum and average total time in system
	protected Tally timeInSystem;

    /**
     * Model constructor.
     *
     * @param owner the model that manages this one (null if no such model)
     * @param modelName this model's name
     * @param showInReport flag to produce output to report file
     * @param showInTrace flag to produce output to trace file
     */
    public MachineProcessModel(Model owner, String modelName,
                    boolean showInReport, boolean showInTrace) {
        super(owner, modelName, showInReport, showInTrace);
    }

    /**
     * Returns a description of the model to be used in the report.
     * @return model description as a string
     */
    public String description() {
        return "This Model describes a Machine shop with refining included using SimProcesses.";
    }

    /**
     * Creates the initial processes in the system and activates them.
     */
    public void doInitialSchedules() {
        // Create the first part process 
    	generator = new Generator(this, "Simulation Generator", true, this);
    	idleProcessorQueue.insert(new Processor(this, "Processor", true));
    	idleInspectorQueue.insert(new Inspector(this, "Inspector", true));
    }

    /**
     * Initializes static model components like state variables, structures
     * (e.g., queues), statistical trackers, and distributions (sources of
     * randomness).
     */
    public void init() {
        // Initialize our state variables and structures

		// statistical counters
		totalParts = new Count(this, "Total Parts in System", true, false);
		timeInSystem = new Tally(this, "Waiting times in System", true, false);
		pUtil = new Accumulate(this, "Processor Utilization", true, false);
		iUtil = new Accumulate(this, "Inspector Utilization", true, false);

		processingQueue = new ProcessQueue<Part>(this, "Processing Queue", true, false);
		inspectionQueue = new ProcessQueue<Part>(this, "Inspection Queue", true, false);
		
		idleProcessorQueue = new ProcessQueue<Processor>(this, "Processors Queue", true, false);
		idleInspectorQueue = new ProcessQueue<Inspector>(this, "Inspector Queue", true, false);
		interArrivalTimes = new ContDistExponential (this, "Interarrival Times", 7, true, false);
		processingTimes = new ContDistExponential(this, "Processing Times", 4.5, true, false);
		inspectionTimes = new ContDistNormal(this, "Inspection Times", 5, 1, true, false);
		inspectionTimes.setNonNegative(true);
		refiningTimes = new ContDistExponential(this, "Processing Times", 3, true, false);
		needsRefining = new BoolDistBernoulli(this, "Needs Refinement", .25, true, false);
    }
    
    public double getInterArrivalTime() {
    	return interArrivalTimes.sample();
    }
    public double getProcessingTime() {
    	return processingTimes.sample();
    }
    public double getInspectionTime() {
    	return inspectionTimes.sample();
    }
    public double getRefineTime() {
    	return refiningTimes.sample();
    }
    public boolean getNeedsRefining() {
    	return needsRefining.sample();
    }
    
    /**
     * Runs the model.
     *
     * @param args is an array of command-line arguments (ignored here)
     */
    public static void main(String[] args) {
    	Experiment.setReferenceUnit(TimeUnit.MINUTES);

		MachineProcessModel model = new MachineProcessModel(null, "Machine Model", true, true);

		Experiment exp = new Experiment("MachineShopExperiment");

		model.connectToExperiment(exp);
		exp.setShowProgressBar(false);
		exp.stop(new TimeInstant(24, TimeUnit.HOURS));

		exp.tracePeriod(new TimeInstant(0, TimeUnit.MINUTES), new TimeInstant(1440, TimeUnit.MINUTES));

		exp.debugPeriod(new TimeInstant(0, TimeUnit.MINUTES), new TimeInstant(60, TimeUnit.MINUTES));
		exp.start();

		exp.report();

		exp.finish();
    }
}
