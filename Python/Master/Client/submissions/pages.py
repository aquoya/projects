import socket as s
import datetime
import os
from ezgui import EzWindow

"""LoginPage Class creates a new EzWindow with all of the necessary tools to create EzSchedules login page."""

class LoginPage(EzWindow):

    LOGIN_PROMPT = "Please log in with your username and password.\n \
        If you wish to create an account, click \"Create an account\" \n \
        after you enter your desired username/password"

    def __init__(self, sock):
        EzWindow.__init__(self, "Login Window")
        self.sock = sock
        self.main()

    def main(self):
        self.label(row=0, col=0, text=LoginPage.LOGIN_PROMPT)
        self.label(row=1, col=0, text="Username: ", fontsize=18)
        self.label(row=2, col=0, text="Password: ", fontsize=18)
        self.username_ref = self.entry(row=1, col=1)
        self.password_ref = self.secret(row=2, col=1)
        self.button(row=3, col=0, command=self.signin, text="Sign in")
        self.button(row=4, col=0, command=self.signup, text="Sign up")
        self.button(row=5, col=0, command=self.debug, text="DEBUG")
        self.button(row=6, col=0, command=self.checkconnection, text="Test Connection")
        self.button(row=7, col=0, command=self.sendcode, text="Submit Code")
        self.run()

# CLIENT METHODS

    def signin(self):
        self.clean()
        print "@TODO encrypt and send credentials to server."

    def signup(self):
        self.clean()
        print "@TODO encrypt and check credentials with server (for duplications)."

    def clean(self):
        self.clearentry(self.username_ref)
        self.clearentry(self.password_ref)

# DEVELOPER METHODS

    def debug(self):
        self.clean()
        CasualPage()

    def checkconnection(self):
        print "Checking connection"
        self.sock.send("debug_check_connection")

    def sendcode(self):
        self.clean()
        SubmitCodePage(self.sock)

"""CasualPage Class creates a new EzWindow with all of the necessary tools to create EzSchedules Casual setup page."""

class CasualPage(EzWindow):

    GUIDE_PROMPT = "@TODO: Description of window"

    def __init__(self):
        EzWindow.__init__(self, "Casual Usage")
        self.main()

    def main(self):
        self.label(row=0, col=0, text=CasualPage.GUIDE_PROMPT)
        self.button(row=1, col=0, command=self.create, text="Create Schedule")
        self.button(row=2, col=0, command=self.confirm, text="Confirm Schedule")
        self.button(row=3, col=0, command=self.view, text="View Schedule")
        self.run()

    def confirm(self):
        print "@TODO: confirm method: Confirm that <the user> schedule is right and send to all recipients."

    def view(self):
        print "@TODO: view method: View <the user> currently scheduled times with all connected people (friends)."

    def create(self):
        CalendarPage()

"""CalendarPage Class creates a new EzWindow with all of the necessary tools to create EzSchedules Calendar Page."""

class CalendarPage(EzWindow):

    def __init__(self):
        EzWindow.__init__(self, "Calendar")
        self.main()

    def main(self):
        dt = datetime.datetime.now() # NOW
        daysinmonth = self.setdays(dt.month, dt.year) # Sets the amount days that occur in that month.
        dayofmonth = datetime.datetime.today().day # Gets current day of the month.
        dayofweek = datetime.datetime.today().weekday() # Gets current day of the week (0 being monday, 6 being sunday)

        # Algorithm to properly draw calendar
        dayofweek -= (dayofweek - 1)

        row = 0
        for calday in range(1, daysinmonth + 1):
            col = dayofweek
            if calday < dayofmonth:
                self.button(row=row, col=col, command=self.viewtime, bgc="gray", text=self.getweekdayasname(col, calday), width=7, height=7)
            elif calday == dayofmonth:
                self.button(row=row, col=col, command=self.changetime, bgc="green", text=self.getweekdayasname(col, calday), width=7, height=7)
            else:
                self.button(row=row, col=col, command=self.changetime, bgc="white", text=self.getweekdayasname(col, calday), width=7, height=7)

            dayofweek += 1
            if dayofweek >= 7:
                dayofweek = 0
                row+=1
        self.run()

    def viewtime(self):
        print "@TODO: viewtime method: Pull up a window that gives <the user> the plans that happened that day"

    def changetime(self):
        print "@TODO: changetime method: Pull up a window that gives <the user> options to set their times up for a day."

    def getweekdayasname(self, dayofweek, dayofmonth):
        daysofweek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
        return daysofweek[dayofweek] + "\n" + str(dayofmonth)

    def setdays(self, month, year):
        if month is 2:
            if year % 4 == 0 :
                return 29
            return 28
        if month <= 7:
            if self.isodd(month):
                return 31
            else:
                return 30
        elif self.isodd(month):
            return 30
        else:
            return 31

    def isodd(self, num):
        x = num % 2
        return x is 1

"""SubmitCodePage Class creates a new EzWindow that is available to all users that are helping development of the project. (Aquoya and Alex)"""

class SubmitCodePage(EzWindow):

    # Constructor that initializes the window and calls main method
    def __init__(self, sock):
        EzWindow.__init__(self, "Submit")
        self.sock = sock
        self.main()

    # Main method that creates the layout of the page.
    def main(self):
        self.label(0, 0, "PROVIDE NAME OF FILE HERE BEFORE SUBMITTING")
        self.name_ref = self.entry(row=0, col=1, width=25)
        self.label(1, 0, "Make sure files you wish to add\nare in the \'submissions\' folder")
        self.filename_ref = self.entry(row=1, col=1, width=25)
        self.button(2, 1, self.sendfile, text="Press to send file")
        self.label(3, 0, "WARNING: Sending file with the same name WILL overwrite the previous file.\n \
            Once the file is sent, it cannot be received until I (Travis) can send it to you.\n \
            Be sure to keep a copy of every file you submit to the server to avoid potential issues. ")

    # Send file method that is attached to the "Press to send file" button where once pressed checks to see if the path exists, if exists then sends to server.
    def sendfile(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        fpath = dir_path + '/submissions/'
        file = self.getentry(self.filename_ref)
        if not os.path.isfile(fpath + file):
            return
        self.sock.send("code_submission")
        self.sock.send(self.getentry(self.name_ref))
        try:
            f = open(fpath + file, 'rb')
            l = f.read(1024)
            while l:
                ready = True if self.sock.recv(1024) == "ready" else False
                if ready:
                    self.sock.send(l)
                    l = f.read(1024)
                else:
                    break
            f.close()
            self.sock.send("end_file")
            print "Successfully sent " + file
        except Exception as e:
            print("Error while sending file")
            print e
        finally:
            self.clean()

    # Cleans the entry fields that are provided in the label.
    def clean(self):
        self.clearentry(self.filename_ref)
        self.clearentry(self.name_ref)
