import socket as s
import datetime
import os
from functools import partial
from copy import deepcopy
from ezgui import EzWindow

""" LoginPage class (extends EzWindow)\
Creates new EzGUI page that contains the login page and developer tools. \
"""

class LoginPage(EzWindow):

    LOGIN_PROMPT = "Please log in with your username and password.\n \
        If you wish to create an account, click \"Create an account\" \n"

    def __init__(self, sock):
        EzWindow.__init__(self, "Login Window")
        self.sock = sock
        self.main()

    def main(self):
        self.label(row=0, col=0, text=LoginPage.LOGIN_PROMPT)
        self.label(row=1, col=0, text="Username: ", fontsize=18)
        self.label(row=2, col=0, text="Password: ", fontsize=18)
        self.username_ref = self.entry(row=1, col=1)
        self.password_ref = self.secret(row=2, col=1)
        self.button(row=3, col=0, command=self.signin, bgc="green", text="Sign in")
        self.button(row=4, col=0, command=self.signup, bgc="green", text="Sign up")
        #self.button(row=5, col=0, command=self.debug, bgc="yellow" ,text="Testing")
        #self.button(row=6, col=0, command=self.checkconnection, bgc="yellow", text="Test Connection")
        #self.button(row=7, col=0, command=self.sendcode, bgc="red", text="Submit Code")
        self.feedback = self.label(row=3, col=1, text="")
        self.run()

# CLIENT METHODS

    def signin(self):
        if self.getentry(self.username_ref) != "":
            self.sock.send("check_credentials|" + self.getentry(self.username_ref) + "|" + self.getentry(self.password_ref))
            try:
                cmd = self.sock.recv(1024)
                correct = cmd == "credentials_correct"
                if correct:
                    global user
                    user = self.getentry(self.username_ref)
                    self.configwidget(self.feedback, text="Successful", fgc="green", bgc="white")
                    CasualPage(self.sock)
                else:
                    incorrect = cmd == "password_incorrect"
                    if incorrect:
                        self.configwidget(self.feedback, text="Incorrect password", fgc="red", bgc="white")
                    else:
                        self.configwidget(self.feedback, text="Username does not exist", fgc="red", bgc="white")
            except KeyboardInterrupt:
                pass
            finally:
                self.clean()


    def signup(self):
        self.clean()
        AccountPage(self.sock)

    def clean(self):
        self.clearentry(self.username_ref)
        self.clearentry(self.password_ref)

# DEVELOPER METHODS

    def debug(self):
        self.clean()
        CasualPage(self.sock)

    def checkconnection(self):
        print "Checking connection"
        self.sock.send("debug_check_connection")

    def sendcode(self):
        self.clean()
        SubmitCodePage(self.sock)

""" CasualPage class (extends EzWindow)\
Creates new EzGUI page that contains the page for the user to work with their schedules. \
"""
class CasualPage(EzWindow):

    GUIDE_PROMPT = "Click \'Create Schedule\'"

    def __init__(self, sock):
        EzWindow.__init__(self, "Casual Usage")
        self.sock = sock
        self.main()

    def main(self):
        self.label(row=0, col=0, text=CasualPage.GUIDE_PROMPT)
        self.button(row=1, col=0, command=self.create, text="Create Schedule", bgc="green")
        self.button(row=2, col=0, command=self.confirm, text="Confirm Schedule", bgc="red")
        self.button(row=3, col=0, command=self.view, text="View Schedule", bgc="red")
        self.run()

    def confirm(self):
        print "@TODO: confirm method: Confirm that <the user> schedule is right and send to all recipients."

    def view(self):
        print "@TODO: view method: View <the user> currently scheduled times with all connected people (friends)."

    def create(self):
        CalendarPage(self.sock)

""" CalendarPage class (extends EzWindow)\
Creates new EzGUI page that contains the page for the user to select which day they would like to add a new Event. \
"""
class CalendarPage(EzWindow):

    def __init__(self, sock):
        EzWindow.__init__(self, "Calendar")
        self.sock = sock
        self.main()

    def main(self):
        dt = datetime.datetime.now() # NOW
        daysinmonth = self.setdays(dt.month, dt.year) # Sets the amount days that occur in that month.
        dayofmonth = datetime.datetime.today().day # Gets current day of the month.
        dayofweek = datetime.datetime.today().weekday() # Gets current day of the week (0 being monday, 6 being sunday)

        x = dayofmonth
        while x > 2:
            if dayofweek <= 0:
                dayofweek = 6
            else:
                dayofweek -= 1
            x -= 1

        row = 0
        for calday in range(1, daysinmonth + 1):
            col = dayofweek
            date = str(dt.year) + "|" + str(dt.month) + "|" + str(calday) + "|" + str(dayofweek)
            if calday < dayofmonth:
                self.button(row=row, col=col, command=partial(self.viewtime, date), bgc="gray", text=self.getweekdayasname(col) + "\n" + str(calday), width=7, height=7)
            elif calday == dayofmonth:
                self.button(row=row, col=col, command=partial(self.changetime, date), bgc="green", text=self.getweekdayasname(col) + "\n" + str(calday), width=7, height=7)
            else:
                self.button(row=row, col=col, command=partial(self.changetime, date), bgc="white", text=self.getweekdayasname(col) + "\n" + str(calday), width=7, height=7)

            dayofweek += 1
            if dayofweek >= 7:
                dayofweek = 0
                row+=1
        self.button(row=row+1, col=5, command=self.prevmonth, bgc="red", text=self.getmonthasname(dt.month-1), width=7, height=4)
        self.button(row=row+1, col=6, command=self.nextmonth, bgc="red", text=self.getmonthasname(dt.month+1), width=7, height=4)
        self.run()

    def prevmonth(self):
        pass

    def nextmonth(self):
        pass

    def viewtime(self, date):
        info = date.split("|")
        fdate = self.formatteddate(info[0], info[1], info[2], info[3])
        date = info[0] + "|" + info[1] + "|" + info[2]
        DailyViewPage(date=date, fdate=fdate, sock=self.sock, past=True)

    def changetime(self, date):
        info = date.split("|")
        fdate = self.formatteddate(info[0], info[1], info[2], info[3])
        date = info[0] + "|" + info[1] + "|" + info[2]
        DailyViewPage(date=date, fdate=fdate, sock=self.sock)

    def formatteddate(self, year, month, day, dayofweek):
        return self.getweekdayasname(int(dayofweek)) + ", " + str(day) + ", " + self.getmonthasname(int(month)) + ", " + str(year)

    def getweekdayasname(self, dayofweek):
        daysofweek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
        return daysofweek[dayofweek]

    def getmonthasname(self, month):
        daysofmonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        return daysofmonth[(month % 12)-1]

    def setdays(self, month, year):
        if month == 2:
            if year % 4 == 0 :
                return 29
            return 28
        if month <= 7:
            if self.isodd(month):
                return 31
            else:
                return 30
        elif self.isodd(month):
            return 30
        else:
            return 31

    def isodd(self, num):
        x = num % 2
        return x is 1

""" SubmitCodePage class (extends EzWindow)\
Creates new EzGUI page that contains the page for the developers to submit their code to the server. \
"""
class SubmitCodePage(EzWindow):

    # Constructor that initializes the window and calls main method
    def __init__(self, sock):
        EzWindow.__init__(self, "Submit")
        self.sock = sock
        self.main()

    # Main method that creates the layout of the page.
    def main(self):
        self.label(0, 0, "PROVIDE NAME OF FILE HERE BEFORE SUBMITTING")
        self.name_ref = self.entry(row=0, col=1, width=25)
        self.label(1, 0, "Make sure files you wish to add\nare in the \'submissions\' folder")
        self.filename_ref = self.entry(row=1, col=1, width=25)
        self.button(2, 1, self.sendfile, text="Press to send file")
        self.label(3, 0, "WARNING: Sending file with the same name WILL overwrite the previous file.\n \
            Once the file is sent, it cannot be received until I (Travis) can send it to you.\n \
            Be sure to keep a copy of every file you submit to the server to avoid potential issues. ")

    # Send file method that is attached to the "Press to send file" button where once pressed checks to see if the path exists, if exists then sends to server.
    def sendfile(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        fpath = dir_path + '/submissions/'
        file = self.getentry(self.filename_ref)
        if not os.path.isfile(fpath + file):
            return
        self.sock.send("code_submission")
        self.sock.send(self.getentry(self.name_ref))
        try:
            f = open(fpath + file, 'rb')
            l = f.read(1024)
            while l:
                ready = True if self.sock.recv(1024) == "ready" else False
                if ready:
                    self.sock.send(l)
                    l = f.read(1024)
                else:
                    break
            f.close()
            self.sock.send("end_file")
            print "Successfully sent " + file
        except Exception as e:
            print("Error while sending file")
            print e
        finally:
            self.clean()

    # Cleans the entry fields that are provided in the label.
    def clean(self):
        self.clearentry(self.filename_ref)
        self.clearentry(self.name_ref)

""" AccountPage class (extends EzWindow)\
Creates new EzGUI page that prompts the user for signing up for an account. \
"""
class AccountPage(EzWindow):
    #Constructor for user sign up page
    def __init__(self, sock):
        EzWindow.__init__(self, "Create Account")
        self.sock = sock
        self.main()

    # Main method that creates the layout of the page.
    def main(self):
        self.label(row=0, col = 0, text = "Sign up")
        self.label(row=1, col = 0, text = "Type username")
        self.newusername = self.entry(row=1,col=1)
        self.label(row=2, col = 0, text = "Type password")
        self.newpassword = self.secret(row=2,col=1)
        self.label(row=3, col = 0, text = "Retype password")
        self.newpasswordretyped = self.secret(row=3,col=1)
        self.button(row=4, col=0, command=self.createaccount, bgc="green", text="Create Account")

        #labels used for possible formating solution
        self.label(row=1, col = 2, text = " ")
        self.label(row=2, col = 2, text = " ")
        self.label(row=3, col = 2, text = " ")

        #label for possible erors
        self.lbl = self.label(row=5, col=0, text="")

    # createaccount examines the fields in the window and determines if passwords match
    # if the passwords are the same, the credentials are sent serverside for account creation.
    def createaccount(self):
        if(self.getentry(self.newpassword) == self.getentry(self.newpasswordretyped) and self.getentry(self.newpassword) != ""):
            #sends username and password in a single string diliminated by a space
            self.sock.send("create_credentials|{}|{}".format(self.getentry(self.newusername),self.getentry(self.newpassword)))
            created = self.sock.recv(1024) #assumes a string is sent back by server if username was available, 'created' else (anything but 'created' will work)

            if created == "credentials_!created":
                self.configwidget(self.lbl, text="Username not available, please try again.", fgc="red", bgc="white")
            elif created == "credentials_created":
                self.configwidget(self.lbl, text="Account successfully created!", fgc="green", bgc="white")
            else:
                pass

        else:
            self.configwidget(self.lbl, text="Passwords didn't match, please try again", fgc="red", bgc="white")

        self.clean()


    # Cleans the entry fields that are provided in the label.
    def clean(self):
        self.clearentry(self.newusername)
        self.clearentry(self.newpassword)
        self.clearentry(self.newpasswordretyped)

""" DailyViewPage class (extends EzWindow)\
Creates new EzGUI page that contains the page that the user can view to see their current events for that specific day. \
"""
class DailyViewPage(EzWindow):

    def __init__(self, date, fdate, sock, past=False):
        EzWindow.__init__(self, "Daily Viewer")
        self.size()
        self.date = date
        self.fdate = fdate
        self.sock = sock
        self.past = past
        self.main()

    def main(self):
        self.label(row=0, col=0, text=self.fdate, fontsize=24)
        events = self.populateday()
        for i in range(len(events)):
            self.label(i + 1, 0, events[i].tostring())
        if not self.past:
            self.button(row=len(events)+1,col=0,bgc="white",text="Add Event +",command=self.newevent, width = 12, height = 2)
        self.run()

    def populateday(self):
        self.sock.send("request|{}|{}".format(user, self.date))
        events = []
        event = self.sock.recv(1024)
        if event == "no_dates":
            return events
        eventdata = event.split("/event")
        for _ in eventdata:
            if _ == "":
                continue
            a = _.split(":")
            events.append(Event(a[0], self.timeformat(a[1]), self.timeformat(a[2]), user))
        return events

    def newevent(self):
        EventPage(self.sock, self.date)

    def timeformat(self, minutes):
        hours = int(minutes) / 60
        minutes = int(minutes) % int(minutes)
        if minutes < 10:
            minutes = str(minutes) + "0"
        return str(hours) + ":" + str(minutes)

    """
    This window will allow the user to schedule events for the days. On the day in the calender page will bring us here
    @Todo create a drop down selector for the time they would like to select. Possibly a drop down menu for other users
    They would like to include in the event, and set up invites.
    """

""" EventPage class (extends EzWindow)\
Creates new EzGUI page that contains the page that the user enters a new Event. \
"""
class EventPage(EzWindow):

    def __init__(self, sock, date):   #ADDED SOCK
        EzWindow.__init__(self, "New Event")
        self.date = date
        self.sock = sock
        self.label(row=2, col=4, text="Event Name (ex: Go out with Alex and Aquoya)")
        self.label(row=4, col = 4, text = "Event Start Time (ex: 6:30am)")
        self.label(row=6, col = 4, text = "Event End Time (ex: 8:30am)")
        self.nameref = self.entry(row=3, col=4)
        self.startref = self.entry(row=5, col=4)
        self.endref = self.entry(row=7, col=4)
        self.button(row=8, col=4, text="Submit", command=self.create_event)
        self.run()

    def create_event(self):
        name = self.getentry(self.nameref)
        start = self.converttominutes(self.getentry(self.startref))
        end = self.converttominutes(self.getentry(self.endref))
        self.date.replace("|", ":")
        self.sock.send("create_event|{}|{}|{}|{}|{}".format(user, self.date, name, start, end))
        self.clean()

    def converttominutes(self, time):
        times = time.split(":")
        total = 0
        total += int(times[0]) * 60
        total += int(times[1])
        return total

    def clean(self):
        self.clearentry(self.nameref)
        self.clearentry(self.startref)
        self.clearentry(self.endref)

""" Event class \
Creates new Event object that holds name of event, start time, end time, and user who owns the event. \
"""
class Event(object):
    def __init__(self, name, start, end, user = "debug jenkins"):
        self.name = name
        self.start = start
        self.end = end
        self.user = user

    def tostring(self):
        if self.end <= 0:
            str = "{}:  {}".format(self.start, self.name)
        else:
            str = "{} - {}: {}".format(self.start, self.end, self.name)
        return str

# Global variable: the user and password.
user = "Debug Jenkins"
pw = "coolpassword12"
