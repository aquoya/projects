/*
 * CS 351 Program 6 (Random Walk)
 * Aquoya Faust
 * 
 * d = 1: .99216
 * d = 2: 0.73976
 * d = 3: 0.33748
 * d = 4: 0.19397
 * d = 5: 0.13487
 */



import java.util.Random;

public class RandomWalk {

	int[] array;
	Random rand;
	int dimensions;

	public RandomWalk(int dimensions) {
		array = new int[dimensions];
		this.dimensions = dimensions;
		rand = new Random();
	}

	private void step() {
		int d = rand.nextInt(dimensions);
		int one = rand.nextInt(2);
		if (one == 0) {
			array[d] -= 1;
		} else {
			array[d] += 1;
		}
	}

	private boolean walk() {
		int numSteps = 10000;
		for (int i = 0; i < numSteps; i++) {
			step();
			if (atOrigin()) {
				return true;
			}
		}
		return false;
	}

	private boolean atOrigin() {
		for (int i = 0; i < array.length; i++) {
			if (array[i] != 0) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		// odds in the first 10,000 steps it returns to the origin
		int numSims = 100000;
		int[] total = new int[5];
		RandomWalk r;
		for (int dim = 0; dim < total.length; dim++) {
			for (int i = 0; i < numSims; i++) {
				r = new RandomWalk(dim + 1);
				if (r.walk())
					total[dim] += 1;
			}
			System.out.println(1.0 * total[dim] / numSims);
		}
	}

}
