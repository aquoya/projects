import socket as s
import threading as th
import os
import sys

from serverstructures import HashMap

"""Server Class is mainly used on the EzSchedules server, unless for debugging. Handles all connections between users, processes data, and stores data."""

class Server:
    sock = s.socket(s.AF_INET, s.SOCK_STREAM) # IPV4 Protocol with TCP connection
    port_number = 10044

    def __init__(self):
        self.connections = [] # All current connections to the server.

        self.sock.bind(('', Server.port_number)) # Allow what kind of ip addresses can connect through which port (all ip addresses through port 10044)
        self.sock.listen(1) # Begin listening for traffic
        print("Server started")
        print("Configured ip address is 96.40.242.16")

    # Runs multiple instances on threads.
    # Handles each current connection to the server.
    def handler(self, c, a):
        while True:
            try:
                data = c.recv(1024) # Max data received is 1024 bytes
            except: # If data reception was interrupted then an exception is thrown. When the exception is caught, the server prompts the address that was disconnected
                self.close_connection(c, a)
                break
            print data

            # Command
            if(data.startswith("/")):
                spec = data[1:len(data)]
                if spec is "close_connection":
                    self.close_connection(c, a)
                    break
                elif spec is "check_credentials":
                    action = "@TODO check credentials from incoming address"
                elif spec is "create_credentials":
                    action = "@TODO create credentials from incoming address"
                else:
                    action = "@TODO Handle unresolved commands."

    # Listens for incoming connections and creates a thread for that connection.
    def run(self):
        self.running = True
        while self.running:
            c, a = self.sock.accept() # Accepts the connection (c). a = client's address
            self.connections.append(c)
            connection_thread = th.Thread(target=self.handler, args = (c, a)) # Used to consistently check for incoming data through multiple connections
            connection_thread.daemon = True
            connection_thread.start()
        self.sock.listen(0)
        exit()

server = Server()
server.run()
