const mongoose = require('mongoose');

const FontSchema = new mongoose.Schema( {
    category : { type : String, required : true},
    family : { type : String, required : true},
    rule : { type : String, required : true},
    url : { type : String, required : true}
});

const Font = mongoose.model( 'Font', FontSchema);

module.exports = Font;