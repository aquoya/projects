let mongoose = require('mongoose');

let User = require('../models/user');

const bcrypt = require('bcrypt');

async function mockUsers(metaDefaults) {
    
    let users = [
        { email : 'bilbo@mordor.org', password : bcrypt.hashSync('123123123', 10), defaults : metaDefaults},
        { email : 'frodo@mordor.org', password : bcrypt.hashSync('234234234', 10), defaults : metaDefaults },
        { email : 'samwise@mordor.org', password : bcrypt.hashSync('345345345', 10), defaults : metaDefaults },
        { email : 'crumpet@brogie.com', password : bcrypt.hashSync('234234234', 10), defaults : metaDefaults },
        { email : 'oliver@brogie.com', password : bcrypt.hashSync('345345345', 10), defaults : metaDefaults }
    ];

    await User.insertMany(users, (err) => {
        if(err) {
            console.log(err);
        }
    });
}

async function mockData(metaDefaults) {
    await mockUsers(metaDefaults);
}

module.exports = mockData;