const mongoose = require('mongoose');


const MetadataSchema = new mongoose.Schema( {
    fonts : { type : [mongoose.Schema.ObjectId], required : true},
    levels : { type : [mongoose.Schema.ObjectId], required : true},
    defaults : { type : mongoose.Schema.ObjectId, required : true}
});

const Metadata = mongoose.model( 'Metadata', MetadataSchema);

module.exports = Metadata;