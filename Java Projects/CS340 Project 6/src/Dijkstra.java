import java.io.*;
import java.util.*;

public class Dijkstra {
	private class Vertex {
		private EdgeNode edges1;
		private EdgeNode edges2;
		private boolean known;
		private int distance;
		private int previous;

		private Vertex() {
			edges1 = null;
			edges2 = null;
			known = false;
			distance = Integer.MAX_VALUE;
			previous = -1;
		}
	}

	private class EdgeNode {
		private int vertex1;
		private int vertex2;
		private EdgeNode next1;
		private EdgeNode next2;
		private int weight;

		private EdgeNode(int v1, int v2, EdgeNode e1, EdgeNode e2, int w) {
			// PRE: v1 < v2
			vertex1 = v1;
			vertex2 = v2;
			next1 = e1;
			next2 = e2;
			weight = w;
		}
	}

	private Vertex[] g;

	public Dijkstra(int size) {
		//initialize g array with all info
		g = new Vertex[size];
		for(int i = 0; i < size; i++) {
			g[i] = new Vertex();
		}
	}

	public void addEdge(int v1, int v2, int w) {
		// PRE: v1 and v2 are legitimate vertices
		// (i.e. 0 <= v1 < g.length and 0 <= v2 < g.length
		//run through g array for each vertex
		//insert all next1's into binary heap
		//insert all next2's into binary heap this goes elsewhere
		EdgeNode curr = new EdgeNode(v1, v2, null, null, w);
		if(v1 > v2) {
			curr.next1 = g[v1].edges1;
			curr.next2 = g[v2].edges2;
			g[v1].edges1 = curr;
			g[v2].edges2 = curr;
		} else {
			curr = new EdgeNode(v2, v1, null, null, w);
			curr.next1 = g[v2].edges1;
			curr.next2 = g[v1].edges2;
			g[v2].edges1 = curr;
			g[v1].edges2 = curr;
		}
		
	}
	
	private void print() {
		for(int i = 0; i < g.length; i++) {
			Stack<Integer> stack = new Stack<Integer>();
			int before = g[i].previous;
			System.out.print( i + ": ");
			stack.push(i);
			while (before != -1) {
				stack.push(before);
				before = g[before].previous;
			}
			while (!stack.isEmpty()) {
				System.out.print(stack.pop() + " ");
			}
			System.out.println();
		}
	}

	public void printRoutes(int j) {
		// find and print the best routes from j to all other nodes in the graph
		BinaryHeap p = new BinaryHeap(g.length);
		g[j].distance = 0;
		g[j].previous = -1; //TODO change stopping case
		g[j].known = true;
		int prev = j;
		for(int i = 1; i < g.length; i++) {
			EdgeNode e1 = g[prev].edges1;
			while(e1 != null) {
				if(!g[e1.vertex2].known) {
					int dist = g[prev].distance + e1.weight;
					if(dist < g[e1.vertex2].distance) {
						g[e1.vertex2].distance = dist;
						g[e1.vertex2].previous = prev;
						p.insert(e1.vertex2, dist);
					}
				}
				e1 = e1.next1;
			}
			EdgeNode e2 = g[prev].edges2;
			while(e2 != null) {
				if(!g[e2.vertex1].known) {
					int dist = g[prev].distance + e2.weight;
					if(dist < g[e2.vertex1].distance) {
						g[e2.vertex1].distance = dist;
						g[e2.vertex1].previous = prev;
						p.insert(e2.vertex1, dist);
					}
				}
				e2 = e2.next2;
			}
			g[p.getMinNode()].known = true;
			g[p.getMinNode()].distance = p.getMinDistance();
			prev = p.getMinNode();
			p.removeMin();
		}

		print();
	}
	
	
	

	public static void main(String args[]) throws IOException {
		BufferedReader b = new BufferedReader(new FileReader(args[0]));
		String line = b.readLine();
		int numNodes = new Integer(line);
		line = b.readLine();
		int source = new Integer(line);
		System.out.println(source);
		Dijkstra g = new Dijkstra(numNodes);
		line = b.readLine();
		while (line != null) {
			Scanner scan = new Scanner(line);
			g.addEdge(scan.nextInt(), scan.nextInt(), scan.nextInt());
			line = b.readLine();
		}
		g.printRoutes(source);
	}
}
