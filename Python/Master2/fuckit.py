import time
import Tkinter

win = Tkinter.Tk()
win_size = 600
x = 5
y = 60
dx = 3
dy = 2
def check_collisions(oval):
    global dy
    global dx
    pos = canvas.coords(oval)
    if pos[3] >= win_size or pos[1] <= 0:
        dy = -dy
    elif pos[2] >= win_size or pos[0] <= 0:
        dx = -dx
def up(e):
    global dy
    dy -= 1
def down(e):
    global dy
    dy += 1
def left(e):
    global dx
    dx -= 1
def right(e):
    global dx
    dx += 1
def space(e):
    global dx
    global dy
    if e.char == " ":
        dx = 0
        dy = 0
win.geometry("{}x{}".format(win_size, win_size))
canvas = Tkinter.Canvas(win, height = win_size, width = win_size)
canvas.pack()
oval = canvas.create_oval(x, x, y, y, fill = "blue")
win.bind("<Up>", up)
win.bind("<Down>", down)
win.bind("<Left>", left)
win.bind("<Right>", right)
win.bind("<Key>", space)
while True:
    canvas.move(oval, dx, dy)
    check_collisions(oval)
    time.sleep(.01)
    canvas.update()
win.mainloop()
