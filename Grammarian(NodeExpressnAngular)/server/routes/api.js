var express = require('express');
var router = express.Router();
var fs = require('fs');

const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
let User = require("../models/user");
let Font = require("../models/font");
let Colors = require("../models/colors");
let Metadata = require("../models/metadata");
let Game = require("../models/game");
let Defaults = require("../models/defaults");
let Level = require("../models/level");

var wordList = undefined;
let metadata;
var fullMetadata = {};
mongoose.set('useFindAndModify', false);

function error(res) {
  res.status(400).send();
}

async function generateMetadata(defaults) {
    
  metadata = {};

  await Font.find({}, (err, savedData) => {
    metadata.fonts = savedData.map(font => font._id);
    fullMetadata.fonts = savedData;
  });

  await Level.find({}, (err, savedData) => {
    metadata.levels = savedData.map(level => level._id);
    fullMetadata.levels = savedData;
  });

  metadata.defaults = defaults._id;
  fullMetadata.defaults = defaults;

  metadata = new Metadata(metadata);

  //Still need to create metadata object
  await metadata.save((err, data) => {
    fullMetadata._id = data._id;
 //   mock(fullMetadata.defaults._id);
  });
}

function generateFonts() {
  let fonts = [
    {category: "open sans", family: "'Trade Winds'", rule: "Trade Winds", url: "'https://fonts.googleapis.com/css?family=Trade+Winds&display=swap'"},
    {category: "monospace", family: "'Kosugi'", rule: "Kosugi", url: "'https://fonts.googleapis.com/css?family=Kosugi&display=swap'"},
    {category: "open sans", family: "'Odibee Sans'", rule: "Odibee Sans", url: "'https://fonts.googleapis.com/css?family=Odibee+Sans&display=swap'"},
    {category: "open sans", family: "'Open Sans Condensed'", rule: "Open Sans Condensed", url: "'https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap'"},
    {category: "monospace", family: "'Ubuntu'", rule: "Ubuntu", url: "'https://fonts.googleapis.com/css?family=Ubuntu&display=swap'"},
    {category: "monospace", family: "'Lacquer'", rule: "Lacquer", url: "'https://fonts.googleapis.com/css?family=Lacquer&display=swap'"},
    {category: "monospace", family: "'Anton'", rule: "Anton", url: "'https://fonts.googleapis.com/css?family=Anton&display=swap'"},
    {category: "monospace", family: "'Yeon Sung'", rule: "Yeon Sung", url: "'https://fonts.googleapis.com/css?family=Yeon+Sung&display=swap'"}
];
return fonts;


}

function generateColors() {
  let defaultColors = {guessBackground : "#ffffff", wordBackground : "#ff00ff", textBackground : "#00ff00"};
    return defaultColors;

}

function generateLevels() {
  let levels = [{name: "Easy", minLength: 3, maxLength: 5, rounds: 8}, 
            {name: "Medium", minLength: 4, maxLength: 10, rounds: 7}, 
            {name: "Hard", minLength: 9, maxLength: 300, rounds: 6}
    ];
    return levels;
}

async function generateDefaultData(callback) {
 // mongoose.connection.dropDatabase();
  let colors = generateColors();
  let fonts = generateFonts();
  let defaultLevel;
  let defaultFont;
  let complete = 0;
  let levels = {};
  await Level.find({}, (err, savedLevels) => {
    if(savedLevels.length == 0) {
      levels = generateLevels();
      Level.insertMany( levels, (err, data) => {
        if(err) {
          console.log(err);
        } else {
          defaultLevel = data[0];
          complete++;
          if(complete == 3) {
            callback(defaultFont, defaultLevel, colors);
          }
        }
      });
    } else {
      levels = savedLevels;
      defaultLevel = savedLevels[0];
          complete++;
          if(complete == 3) {
            callback(defaultFont, defaultLevel, colors);
          }
    }
    
  });

  await Colors.create(colors, (err, data) => {
    if(err) {
      console.log("Could not load colors");
    } else {
      colors = data;
      complete++;
      if(complete == 3) {
        callback(defaultFont, defaultLevel, colors);
      }
    }
  });

  await Font.find({}, (err, savedFonts) => {
    if(err || savedFonts.length == 0) {
      Font.insertMany(fonts, (err, data) => {
        if(err) {
          console.log("Could not load fonts");
        } else {
          defaultFont = data[0];
          complete++;
          if(complete == 3) {
            callback(defaultFont, defaultLevel, colors);
          }
        }
      });
    } else {
      fonts = savedFonts;
      defaultFont = savedFonts[0];
          complete++;
          if(complete == 3) {
            callback(defaultFont, defaultLevel, colors);
          }
    }
    });
}

async function generateDefaults(defaultFont, defaultLevel, defaultColors) {
  await Defaults.create({ font: defaultFont, level: defaultLevel, colors: defaultColors}, (err, savedData) => {
    generateMetadata(savedData);
  });
}

var main = function() {
  
  //mongoose.connection.dropDatabase(); 
  //metadata = generateDefaultMetadata().catch(console.log);
  
  wordList = populateList("public/assets/wordlist.txt");
  generateDefaultData((defaultFont, defaultLevel, defaultColors) => {
    generateDefaults(defaultFont, defaultLevel, defaultColors);
  });
};

var populateList = function(fileName) {
  //blocking no call back function
  let list = fs.readFileSync(fileName, "utf-8").split("\r\n");
  list.sort((s1, s2) => (s1.length-s2.length)); //Sorts the list, shorter-longer
  return list;
};

var stringToView = function(string) {
  let i = 0;
  let str = "";
  for(i = 0; i < string.length; i++) {
    str += "_";
  }
  return str;
};

var getWord = function(lvl) {
  let sample = wordList.filter( word => {
    return ((word.length >= lvl.minLength) && (word.length <= lvl.maxLength));
  });
  let wordIndex = Math.floor(Math.random() * sample.length);
  return sample[wordIndex];
};


var updateView = function(targetWord, guess, view) {
  let i;
  let retView = "";
  for(i = 0; i < targetWord.length; i++) {
    if(guess == targetWord.charAt(i)) {
      retView += targetWord.charAt(i);
    } else {
      retView += view.charAt(i);
    }
  }
  return retView;
};

router.get("/users", (req, res, next) => {
  User.find({}, (err, data) => {
    if(err) {
      error(res);
    } else {
      res.json(data);
    }
  });
});

router.post("/login", (req, res, next) => {
    let obj = req.body;
    //console.log(req.body);
    req.session.regenerate((err) => {
        if(err) {
            error(res);
        } else {
            let email = obj.email || "";
            let password = obj.password || "";

            User.findOne({ email: email}, (err, user) => {
                 if(err || !user) { //!user is redundant
                    res.status(400).json({msg: "Incorrect Username and Password"});
                } else {
                    bcrypt.compare(password, user.password, (err2, isValid) => {
                        if (err2 || !isValid) {
                            res.status(400).json({msg: "Incorrect Username and Password"});
                        } else {

                            req.session.user = user;
                            let userNoPassword = {
                                email: user.email,
                                _id: user._id,
                                defaults: user.defaults
                            }
                            Defaults.findOne({_id: user.defaults}, (err, saved) => {
                              Font.findOne({_id: saved.font}, (err, savedFont) => {
                                Level.findOne({_id: saved.level}, (err, savedLevel) => {
                                  Colors.findOne({_id: saved.colors}, (err, savedColors) => {
                                    userNoPassword.defaults = saved;
                                    userNoPassword.defaults = {font: savedFont, level: savedLevel, colors: savedColors, _id: saved._id}
                                    res.json(userNoPassword);
                                  });
                                });
                              });
                            });
                            
                        }
                    });
                }
                });
        }
    });

});

router.post('/logout', (req, res) => {
    req.session.destroy( (err) => {
        res.json("ok");
    });
});


router.get("/meta", (req, res, next) => {
  res.json(fullMetadata);
});

router.get("/:userid", (req, res, next) => {
  //if(req.params.userId == req.session.user._id) {
    Game.find({userId: req.params.userid}, (err, data) => {
      res.json(data);
    });
  //}
});

router.post("/:userid", (req, res, next) => {

  let clientGame = {};
  clientGame.level = fullMetadata.levels.filter(a => a.name == req.query.level)[0];
  clientGame.font = fullMetadata.fonts.filter(a => a.family == req.headers["x-font"])[0];
  let word = getWord(clientGame.level);
  clientGame.view = stringToView(word);
  clientGame.guesses = "";
  clientGame.remaining = clientGame.level.rounds;
  clientGame.status = "unfinished";
  Colors.create(req.body, (err, colors) => {
    clientGame.colors = colors;
    Game.create({
      colors: colors._id,
      remaining: clientGame.remaining,
      font: clientGame.font._id,
      level: clientGame.level._id,
      guesses: clientGame.guesses,
      status: clientGame.status,
      target: word,
      view:  clientGame.view,
      userId: req.params.userid
    }, (err, game) => {
      if(err) {
        res.json({msg: "Game could not be created"});
      }
      clientGame._id = game._id;
      clientGame.timestamp = game.timestamp;
      res.json(clientGame);
    });
  });
});

router.put("/:userid/defaults", (req, res, next) => {
  let uid = req.params.userid;
  //console.log(uid, req.session.user._id);
  if(req.session.user._id != uid) {
    res.status(403).send("Error");
  } else {
  let newDefaults = {};
  newDefaults.font = req.body.font;
  newDefaults.level = req.body.level;
  newDefaults.colors = req.body.colors;
  newDefaults.font = fullMetadata.fonts.filter(f => f.family == newDefaults.font)[0];
  newDefaults.level = fullMetadata.levels.filter(l => l.name == newDefaults.level)[0];
  if(newDefaults.colors.wordBackground && newDefaults.colors.guessBackground && newDefaults.colors.textBackground) {
    let col = {wordBackground: newDefaults.colors.wordBackground, textBackground: newDefaults.colors.textBackground, guessBackground:newDefaults.colors.guessBackground};
    newDefaults.color = col;
    Colors.create(col, (err, savedColors) => {
      Defaults.create({colors:savedColors._id, font: newDefaults.font._id, level: newDefaults.level._id}, (err, savedDefaults) => {
        User.findByIdAndUpdate(uid, {defaults: savedDefaults._id}, (err, savedUser) => {
          res.status(200).json(newDefaults);
          res.send();
        });
      });
    });
  }
}
});

router.get("/:userId/:gameId", (req, res, next) => {
  let uid = req.params.userId;
  let gid = req.params.gameId;
  Game.findOne({userId: uid, _id: gid}, (err, data) => {
    Colors.findOne({_id : data.colors}, (err, colorsData) => {
      Font.findOne({_id : data.font}, (err, fontData) => {
        Level.findOne({_id : data.level}, (err, levelData) => {
         // console.log(fontData);
          let game = {
            guesses: data.guesses,
            status: data.status,
            _id: data._id,
            remaining: data.remaining,
            font: fontData,
            colors: colorsData,
            level: levelData,
            timestamp: data.timestamp,
            view: data.view,
            userId: uid
          }
          if(game.status != "unfinished") {
            game.target = data.target;
          }
          //console.log(game);
          res.json(game);
        });
      });
    });
  });
});

router.post("/:userid/:gid/guesses", (req, res, next) => {
  let gid = req.params.gid;
  let userid = req.params.userid;
  let guess = req.query.guess;
  if(!guess) {
    res.status(200).error({msg: "Invalid guess"});
  }
  else if(guess.length != 1) {
    res.status(200).error({msg: "Invalid Guess"});
  }
  Game.findOne({_id: gid, userId: userid}, (err, game) => {
    let tempView = game.view;
    game.view = updateView(game.target, guess, game.view);
    if(game.view != tempView) {
      game.remaining += 1;
    } else {
      game.guesses = game.guesses.concat(guess);
    }
    game.remaining -= 1;
    if(game.view == game.target) {
      game.status = "victory";
      //game.timeToComplete = (new Date()).getTime() - game.timestamp;
    } else if(game.remaining == 0) {
      //game.timeToComplete = (new Data().getTime() - game.timestamp);
      game.status = "loss";
    }
    //need to update game and save other properties.
    Game.findByIdAndUpdate(game._id, {view: game.view, guesses: game.guesses, remaining: game.remaining, status:game.status}, (err, data) => {
        Colors.findOne({_id: data.colors._id}, (err, colorsObj) => {
          let font = fullMetadata.fonts.filter(f => f._id == game.font);
          //console.log(font);
          let level = fullMetadata.levels.filter(l => l._id == game.level);
          let sanitizedGame = {
            _id: game._id,
            font: font,
            view: game.view,
            colors: colorsObj,
            status: game.status,
            level: level,
            guesses: game.guesses,
            timestamp: game.timestamp,
            userId: game.userId,
            remaining: game.remaining
          }
          if(sanitizedGame.status != "unfinished") {
            sanitizedGame.target = data.target;
            //also update timetocomplete
          }
          res.json(sanitizedGame);
      });
    });
  });
});


let mock = require('./mock');
main();

module.exports = router;
