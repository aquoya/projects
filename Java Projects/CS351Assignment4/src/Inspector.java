import co.paralleluniverse.fibers.SuspendExecution;
import desmoj.core.simulator.*;

/**
 * 
 * @author Aquoya Faust
 *
 */
public class Inspector extends SimProcess{

	private MachineProcessModel owner;
	public Inspector(MachineProcessModel owner, String name, boolean showInStackTrace) {
		super(owner, name, showInStackTrace);
		this.owner = owner;
	}

	@Override
	public void lifeCycle() throws SuspendExecution {
		
		while (true) {
			if(!owner.inspectionQueue.isEmpty()) {
				Part p = owner.inspectionQueue.removeFirst();
				TimeSpan ts;
				ts = new TimeSpan(owner.inspectionTimes.sample());
				hold(ts);
				owner.iUtil.update(ts);
				p.activate();
			} else {
				owner.idleInspectorQueue.insert(this);
				passivate();
			}
		}
		
	}

}
