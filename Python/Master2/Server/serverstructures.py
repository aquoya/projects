"""Contains all the data structures necessary to create a Hash Map. *** NEEDS FILE WRITING *** """

class DataNode:

    def __init__(self, key=-1, username="head", password="", data="", next=None):
        self.key = key
        self.aeskey = aeskey
        self.username = username
        self.password = password
        self.data = data

        self.next = next
        self.hasnext = False if next is None else True

class DataList:

    def __init__(self, maxsize):
        self.size = 0
        self.maxsize = maxsize
        self.head = DataNode()

    # Does not need to check for existence as HashMap handles that.
    # If the size of the List does not exceed maxsize (default 10), then adds a node with all of the associated values to the Linked List.
    def add(self, key, username, password, data):
        if self.size >= self.maxsize:
            return
        self.head = Node(key, username, password, data, self.head)
        self.size += 1
    # Does not need to check for existence as HashMap handles that.
    # Finds the key in the Linked List to remove it completely from the list.
    def remove(self, key):
        temp = self.head
        if temp.key == key:
            self.head = temp.next
            del temp
            return

        while temp.next.hasnext:
            if temp.next.key == key:
                break
            temp = temp.next

        removing = temp.next
        temp.next = temp.next.next
        del removing
        self.size -= 1

    # Checks to see if the key exists in the Linked List
    def exists(self, key):
        if self.head.username is "head":
            return False
        keylist = []
        temp = self.head
        while temp.hasnext:
            keylist.append(temp.key)
            temp = temp.next
        if key in keylist:
            return True
        return False

    # Does not need to check for existence as HashMap handles that.
    # Finds the key in the linked list to return all data associated with that key.
    def getinfo(self, key):
        temp = self.head
        while temp.hasnext:
            if key == temp.key:
                break
            temp = temp.next
        return temp.username, temp.password, temp.data

class HashMap:

    def __init__(self, maxsize=100, maxlistsize=10):
        dlist = DataList(maxlistsize)
        self.maxsize = maxsize
        self.map = [dlist] * maxsize

    def add(self, key, username, password, data):
        hashcode = key % self.maxsize
        if self.map[hashcode].exists(key):
            return
        self.map[hashcode].add(key, username, password, data)

    def remove(self, key):
        hashcode = key % self.maxsize
        if not self.map[hashcode].exists(key):
            return
        self.map[hashcode].remove(key)

    def exists(self, key):
        hashcode = key % self.maxsize
        if self.map[hashcode].exists(key):
            return True
        return False

    def getaeskey(self, key):
        hashcode = key % self.maxsize
        if self.exists(key):
            username, password, data = self.map[hashcode].getinfo(key)
        else:
            return -1
        return aeskey

    def getusername(self, key):
        hashcode = key % self.maxsize
        if self.exists(key):
            username, password, data = self.map[hashcode].getinfo(key)
        else:
            return 'dne'
        return username

    def getpassword(self, key):
        hashcode = key % self.maxsize
        if self.exists(key):
            username, password, data = self.map[hashcode].getinfo(key)
        else:
            return 'dne'
        return password

    def getdata(self, key):
        hashcode = key % self.maxsize
        if self.exists(key):
            username, password, data = self.map[hashcode].getinfo(key)
        else:
            return 'dne'
        return data

    def getinfo(self, key):
        hashcode = key % self.maxsize
        if self.exists(key):
            username, password, data = self.map[hashcode].getinfo(key)
        else:
            return 'dne', 'dne', 'dne'
        return username, password, data
