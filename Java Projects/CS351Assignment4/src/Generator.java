import co.paralleluniverse.fibers.SuspendExecution;
import desmoj.core.dist.BoolDistBernoulli;
import desmoj.core.dist.ContDistUniform;
import desmoj.core.simulator.*;

/**
 * 
 * @author Aquoya Faust
 *
 */
public class Generator extends SimProcess{
	
	private MachineProcessModel model;
	private Model owner;
	public Generator(Model owner, String name, boolean showInTrace, MachineProcessModel model) {
		super(owner, name, showInTrace);
		this.model = model;
		this.owner = owner;
		this.activate(new TimeInstant(0));
	}

	@Override
	public void lifeCycle() throws SuspendExecution {
		while (true) {
			///sample arrival time.
			double currInterArrivalTime = model.getInterArrivalTime();
			double aTime = model.presentTime().getTimeAsDouble();
			//Pass in a value for each of the events except for refinign times. Do not need to gen value unless the bernoulli is true;
			Part part = new Part(owner, "part", true, model, aTime);
			hold(new TimeSpan(currInterArrivalTime));
			part.activate();
		}
	}

}
