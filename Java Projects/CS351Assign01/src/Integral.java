/*
 * CS 351 Program 2 (Integral Calculation)
 * Aquoya Faust
 * 
 * 
 * integral = 1.3304... random after 1.33
 * 
 */

import java.util.Random;

public class Integral {
	
	int numSims;
	public Integral(int numSims) {
		this.numSims = numSims;
	}
	//integral 0-pi of sin(2x)cos(x)dx
	private double solveX(double x) {
		//This method will return the y value of the function for comparison
		double result = Math.sin(2*x)*Math.cos(x);
		return result;
	}

	public static void main(String[] args) {
		Random rand = new Random();
		Integral f = new Integral(100000);
		int total = 0;
		for (int i = 0; i < f.numSims; i++) {
			double randX = rand.nextDouble()*Math.PI;
			double randY = rand.nextDouble()*Math.PI/4;
			if(randY <= f.solveX(randX)) {
				total += 1;
			}
		}
		System.out.println(1.0 * total/f.numSims * Math.PI * Math.PI/4);

	}

}
