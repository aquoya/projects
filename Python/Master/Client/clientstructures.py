from Tkinter import Entry

"""ReferenceList/ReferenceMap - Holds all references to Entries for EzGui"""

class Node:

    def __init__(self, key, entry, next):
        self.key = key
        self.entry = entry
        self.next = next
        self.hasnext = False if next is None else True


class ReferenceList:

    def __init__(self):
        self.head = None

    def insert(self, key, entry):
        self.head = Node(key, entry, self.head)

    def remove(self, key):
        if self.head == None:
            return

        if self.head.key == key:
            self.head = None
            return

        temp = self.head
        while temp.hasnext:
            if temp.next.key == key:
                temp.next = temp.next.next
                break
            temp = temp.next
            print temp

    def get(self, key):
        if self.head is None:
            return None
        temp = self.head
        while temp != None:
            if temp.key == key:
                return temp.entry
            temp = temp.next
        return None


class ReferenceMap:

    def __init__(self, size):
        self.map = [ReferenceList()] * size
        self.size = size

    def insert(self, entry):
        key = hash(entry)
        hashcode = hash(entry) % self.size
        self.map[hashcode].insert(key, entry)
        return key

    def remove(self, key):
        hashcode = key % self.size
        self.map[hashcode].remove(key)

    def get(self, key):
        hashcode = key % self.size
        return self.map[hashcode].get(key)
