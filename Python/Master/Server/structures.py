"""Contains all the data structures necessary to create a Hash Map. """

# Used in the DataList for hash map
class DataNode(object):

    def __init__(self, key=-1, username="", password="", data="", next=None):
        self.key = key
        self.username = username
        self.password = password
        self.data = data

        self.next = next
        self.hasnext = False if next is None else True

# Used in the hash map
class DataList(object):

    def __init__(self, maxsize):
        self.size = 0
        self.maxsize = maxsize
        self.head = None

    # Does not need to check for existence as HashMap handles that.
    # If the size of the List does not exceed maxsize (default 10), then adds a node with all of the associated values to the Linked List.
    def add(self, key, username, password, data):
        if self.size >= self.maxsize:
            return
        self.head = DataNode(key, username, password, data, self.head)
        self.size += 1

    # Does not need to check for existence as HashMap handles that.
    # Finds the key in the Linked List to remove it completely from the list.
    def remove(self, key):
        temp = self.head
        if temp == None:
            return
        if temp.key == key:
            self.head = temp.next
            del temp
            return

        while temp.next != None:
            if temp.next.key == key:
                break
            temp = temp.next

        removing = temp.next
        temp.next = temp.next.next
        del removing
        self.size -= 1

    def getnode(self, key):
        temp = self.head
        if temp == None:
            return None

        while temp != None:
            if temp.key == key:
                return temp
        return None

    # Checks to see if the key exists in the Linked List
    def exists(self, key):
        keylist = []
        temp = self.head
        while temp != None:
            keylist.append(temp.key)
            temp = temp.next
        if key in keylist:
            return True
        return False

    # Does not need to check for existence as HashMap handles that.
    # Finds the key in the linked list to return all data associated with that key.
    def getinfo(self, key):
        temp = self.head
        while temp != None:
            if key == temp.key:
                break
            temp = temp.next
        return temp.username, temp.password, temp.data

    def getprintlist(self):
        temp = self.head
        s = ""
        while temp != None:
            s += str(temp.key) + ": " + temp.username + ", " + temp.password + ", " + temp.data + "\n"
            temp = temp.next
        return s

# Hash map that stores all of the data necessary to maintain EzSchedules
class CredentialsMap(object):

    def __init__(self, maxsize=100, maxlistsize=10):
        self.maxsize = maxsize
        self.map = [DataList(maxlistsize) for _ in range(maxsize)]

    def __str__(self):
        s = ""
        for i in range(len(self.map)):
            s += self.map[i].getprintlist()
        return s

    def add(self, key, username, password, data):
        hashcode = (key % self.maxsize)
        if self.map[hashcode].exists(key):
            return
        self.map[hashcode].add(key, username, password, data)

    def remove(self, key):
        hashcode = (key % self.maxsize)
        if not self.map[hashcode].exists(key):
            return
        self.map[hashcode].remove(key)

    def edit(self, key, data):
        hashcode = (key % self.maxsize)
        if not self.map[hashcode].exists(key):
            return
        node = self.map[hashcode].getnode(key)
        if node == None:
            return
        node.data += data

    def exists(self, key):
        hashcode = (key % self.maxsize)
        if self.map[hashcode].exists(key):
            return True
        return False

    def getusername(self, key):
        hashcode = (key % self.maxsize)
        if self.exists(key):
            username, password, data = self.map[hashcode].getinfo(key)
        else:
            return 'dne'
        return username

    def getpassword(self, key):
        hashcode = (key % self.maxsize)
        if self.exists(key):
            username, password, data = self.map[hashcode].getinfo(key)
        else:
            return 'dne'
        return password

    def getdata(self, key):
        hashcode = (key % self.maxsize)
        if self.exists(key):
            username, password, data = self.map[hashcode].getinfo(key)
        else:
            return 'dne'
        return data

    def getinfo(self, key):
        hashcode = (key % self.maxsize)
        if self.exists(key):
            username, password, data = self.map[hashcode].getinfo(key)
        else:
            return 'dne', 'dne', 'dne'
        return username, password, data

    def iterator(self):
        return CredentialsIterator(self.map)

# Iterates through the Hash map to retrieve each set of data.
class CredentialsIterator(object):

    def __init__(self, map):
        self.pos = 0
        self.map = map
        self.idx = map[0].head

    def next(self):
        node = self.idx
        self.idx = self.idx.next
        return node.username, node.password, node.data

    def hasnext(self):
        if self.idx == None:
            self.pos += 1
            if self.pos >= len(self.map):
                return False
            self.idx = self.map[self.pos].head
            return self.hasnext()
        return True
