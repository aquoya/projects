import co.paralleluniverse.fibers.SuspendExecution;
import desmoj.core.simulator.SimProcess;
import desmoj.core.simulator.TimeSpan;

/**
 * 
 * @author Aquoya Faust
 *
 */
public class Processor extends SimProcess{
	
	private MachineProcessModel owner;
	public Processor(MachineProcessModel owner, String name, boolean showInStackTrace) {
		super(owner, name, showInStackTrace);
		this.owner = owner;
		System.out.println("");
		System.out.println();
	}
	
	@Override
	public void lifeCycle() throws SuspendExecution {
		while (true) {
			if(!owner.processingQueue.isEmpty()) {
				Part p = owner.processingQueue.removeFirst();
				TimeSpan ts;
				if(!p.needsRefining())
					ts = new TimeSpan(owner.processingTimes.sample());
				else
					ts = new TimeSpan(owner.refiningTimes.sample());
				hold(ts);
				owner.pUtil.update(ts); //hold and increment by timespan
				p.activate();
			} else {
				owner.idleProcessorQueue.insert(this);
				passivate();
			}
		}
	}

}
