import co.paralleluniverse.fibers.SuspendExecution;
import desmoj.core.dist.BoolDistBernoulli;
import desmoj.core.dist.ContDistUniform;
import desmoj.core.simulator.*;


/**
 * 
 * @author Aquoya Faust
 * Part a simprocess that models a part in a system
 */
public class Part extends SimProcess{
	
	private MachineProcessModel model;
	private double arrivalTime;
	private boolean needsRefining;
	public Part(Model owner, String name, boolean showInTrace, MachineProcessModel model, double arrivalTime) {
        super(owner, name, showInTrace);
        this.model = model;
        this.arrivalTime = arrivalTime;
		this.needsRefining = model.needsRefining.sample();
	}
	
	public boolean needsRefining() {
		return needsRefining;
	}

	@Override
	public void lifeCycle() throws SuspendExecution {
		
		model.totalParts.update(1);
		
		processorCycle();
		//start of inspector Queue
		model.inspectionQueue.insert(this);
		if(!model.idleInspectorQueue.isEmpty()) {
			model.idleInspectorQueue.removeFirst().activate();
		}
		passivate();
		
		if(needsRefining) {
			processorCycle();
		}
		//Item is done holding now we pop the next one from the queue
		//assuming it exists
		double partTime = model.presentTime().getTimeAsDouble() - arrivalTime;
		model.totalParts.update(-1);
		model.timeInSystem.update(partTime);
	}
	
	private void processorCycle() throws SuspendExecution{
		model.processingQueue.insert(this);
		
		if(!model.idleProcessorQueue.isEmpty()) {
			model.idleProcessorQueue.removeFirst().activate(); //activate our processor
		} 
		passivate();
	}
	

}
