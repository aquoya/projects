const mongoose = require('mongoose');

const LevelSchema = new mongoose.Schema( {
    rounds : { type : Number, required : true},
    maxLength : { type : Number, required : true},
    minLength : { type : Number, required : true},
    name : { type : String, required : true}
});

const Level = mongoose.model( 'Level', LevelSchema);

module.exports = Level;