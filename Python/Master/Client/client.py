import socket as s
import threading as th

import ezgui
import pages as PAGES

"""Client Class is the where the User mainly operates off of. Processes all of the commands coming from the server and controls communications between all connected users."""

class Client:
    sock = s.socket(s.AF_INET, s.SOCK_STREAM) # IPV4 Protocol with TCP connection
    port_number = 10044

    def __init__(self, address):
        self.sock.connect((address, Client.port_number)) # Connect to the given ip address through configured port 10044
        print "Successfully connected to server"
        recv_thread = th.Thread(target=self.sc_transfer)
        recv_thread.daemon = True
        recv_thread.start()

        lp = PAGES.LoginPage(self.sock)
        self.action("close_connection")

    def sc_transfer(self):
        while(True):
            data = self.sock.recv(1024) # Receive up to 1024 bytes of data.
            self.action(data)

    def action(self, data):
        if data == "close_connection":
            self.sock.send("close_connection")
            self.sock.close()
            exit()
        elif data == "debug_check_connection_received":
            print "Connection successfully received from server."


address = "138.49.184.109" #CHANGE THIS IF WISH TO CHANGE SERVER ADDRESS
client = Client(address)
