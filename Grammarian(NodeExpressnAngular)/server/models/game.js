const mongoose = require('mongoose');

const GameSchema = new mongoose.Schema( {
    colors : { type : mongoose.Schema.ObjectId, required : true},
    font : { type : mongoose.Schema.ObjectId, required : true},
    guesses : { type : String, default : ""},
    level : { type : mongoose.Schema.ObjectId, required : true},
    remaining : { type : Number, required : true},
    status : { type : String, default : "unfinished"},
    target : { type : String, required : false},
    timestamp : { type : Date, default : Date.now },
    timeToComplete : { type : Number, required : false},
    view : {type : String, required : true},
    userId: {type: mongoose.Schema.ObjectId, required : true}
});

const Game = mongoose.model( 'Game', GameSchema );

module.exports = Game;