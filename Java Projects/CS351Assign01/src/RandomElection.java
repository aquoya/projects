/**
 * CS 351 Problem 5 (Random Election)
 * 
 * Probability of Candidate being up the entire time:
 * (Odds): %of votes
 * (0.0077): 51%
 * (0.3094): 60%
 * (0.5549): 70%
 * (0.7281): 80%
 * (0.8768): 90%
 * (0.937): 95%
 */

import java.util.Random;

public class RandomElection {
	double probability;

	public RandomElection(double probability) {
		this.probability = probability;
	}

	private boolean election() {
		int numVotes = 10000;
		Random random = new Random();
		int upVote = 0;
		int downVote = 0;
		for (int i = 0; i < numVotes; i++) {
			if ((1 + random.nextInt(100)) < probability * 100) {
				upVote += 1;
			} else {
				downVote += 1;
			}
			if (downVote > upVote) {
				return false;
			}
		}
		return true;
		

	}

	public static void main(String[] args) {
		int numSims = 10000;
		RandomElection r51 = new RandomElection(.51);
		RandomElection r6 = new RandomElection(.6);
		RandomElection r7 = new RandomElection(.7);
		RandomElection r8 = new RandomElection(.8);
		RandomElection r9 = new RandomElection(.9);
		RandomElection r95 = new RandomElection(.95);
		int[] totals = new int[6];
		for (int i = 0; i < numSims; i++) {
			if (r51.election()) {
				totals[0] += 1;
			}
			if (r6.election()) {
				totals[1] += 1;
			} 
			if (r7.election()) {
				totals[2] += 1;
			}
			if (r8.election()) {
				totals[3] += 1;
			}
			if (r9.election()) {
				totals[4] += 1;
			} 
			if (r95.election()) {
				totals[5] += 1;
			}
		}
		for (int i = 0; i < 6; i++)
			System.out.println(1.0 * totals[i]/numSims);
	}

}
