import socket as s
import threading as th
import datetime as dt
import os
import sys
import time
from structures import CredentialsMap, CredentialsIterator

"""Server Class is mainly used on the EzSchedules server, unless for debugging. Handles all connections between users, processes data, and stores data."""

class Server:
    sock = s.socket(s.AF_INET, s.SOCK_STREAM) # IPV4 Protocol with TCP connection
    port_number = 10044

    def __init__(self):
        self.connections = [] # All current connections to the server.

        self.createmaps()

        self.sock.setsockopt(s.SOL_SOCKET, s.SO_REUSEADDR, 1) # Used during debugging, previous server instances would occupy that IP address preventing this instance from opening.
        self.sock.bind(('', Server.port_number)) # Allow what kind of ip addresses can connect through which port (all ip addresses through port 10044)
        self.sock.listen(1) # Begin listening for traffic
        print("Server started")

# CORE METHODS

    # Runs multiple instances on threads.
    # Handles each current connection to the server.
    def handler(self, c, a):
        print str(a[0]) + ":" + str(a[1]) + " connected"
        self.log(cmd='connected', a=a)
        while True:
            try:
                data = c.recv(1024) # Max data received is 1024 bytes
            except Exception as e: # If data reception was interrupted then an exception is thrown. When the exception is caught, the server prompts the address that was disconnected
                c.close()
                self.log(cmd=e)
                break
            # Command
            if self.process(data=data, a=a, c=c):
                break

    # Listens for incoming connections and creates a thread for that connection.
    def run(self):
        self.running = True
        while self.running:
            c, a = self.sock.accept() # Accepts the connection (c). a = client's address
            self.connections.append(c)
            connection_thread = th.Thread(target=self.handler, args = (c, a)) # Used to consistently check for incoming data through multiple connections
            connection_thread.daemon = True
            connection_thread.start()
        self.sock.listen(0)
        exit()

    # Compares data to the known actions that the server handles, returns True to break the loop, else False to continue.
    def process(self, data, a, c):
        self.console("From " + str(a[0]) + ":" + str(a[1]) + ": " + data)
        splitdata = data.split("|")
        if splitdata[0] == "close_connection":
            self.console("Connection " + str(a[0]) + ":" + str(a[1]) + " closed.")
            self.log(cmd='disconnected', a=a)
            return True
        elif splitdata[0] == "check_credentials":
            self.checkcredentials(splitdata[1], splitdata[2], c)
            self.log(cmd="check_credentials", a=a)
        elif splitdata[0] == "create_credentials":
            self.createcredentials(splitdata[1], splitdata[2], c)
            self.log(cmd="create_credentials", a=a)
        elif splitdata[0] == "debug_check_connection":
            self.checkconnection(a, c)
            self.log(cmd='check_connection', a=a)
        elif splitdata[0] == "code_submission":
            self.receivecode(c)
            self.log(cmd='code_submission', a=a)
        elif splitdata[0] == "request":
            self.getdata(splitdata[1], splitdata[2], splitdata[3], splitdata[4], c)
            self.log(cmd='request_events', a=a)
        elif splitdata[0] == "create_event":
            self.enterdata(splitdata[1], splitdata[2], splitdata[3], splitdata[4], splitdata[5], splitdata[6], splitdata[7])
            self.log(cmd='create_event', a=a)
        else:
            c.send("Error with data transfer, closing connection.")
            c.close()
            self.log(cmd="DATA TRANSFER ERROR")
            return True
        return False

# CLIENT CONTROL METHODS

    # Enters the data given by the client to the server's database.
    def enterdata(self, user, year, month, day, name, start, end):
        time.sleep(.5)
        key = hash(user)
        date = year + month + day

        if self.credentials.exists(key):
            data = "/date" + date + "/event" + name + ":" + start + ":" + end
            self.credentials.edit(key, data)
            self.maptofile("credentials.txt")

    # Retrieves the data from the server's database and sends it to the client.
    def getdata(self, user, year, month, day, c):
        time.sleep(.5)
        key = hash(user)

        date = year + month + day

        if self.credentials.exists(key):
            data = self.credentials.getdata(key)
            if data == "":
                c.send("no_dates")
                return
            splitdata = data.split("/date")
            allinfo = ""
            for info in splitdata:
                if date in info:
                    allinfo += info.split(date, 1)[-1]
            if "/event" not in allinfo:
                c.send("no_dates")
            else:
                c.send(allinfo)
        else:
            c.send("no_dates")

    # Checks the credentials received from the client to see if the username is incorrect, the password is incorrect, or if both are correct.
    def checkcredentials(self, username, password, c):
        key = hash(username)
        time.sleep(.5)
        if self.credentials.exists(key):
            if self.credentials.getpassword(key) == password:
                c.send("credentials_correct")
            c.send("password_incorrect")
        else:
            c.send("credentials_!correct")

    # Creates the credentials received from the client to see if the username is taken or not.
    def createcredentials(self, username, password, c):
        key = hash(username)
        time.sleep(.5)
        if self.credentials.exists(key):
            c.send("credentials_!created")
        else:
            c.send("credentials_created")
            self.mapcredentials(username, password, "")

# ORGANIZATIONAL METHODS

    # Maps the credentials created by a user to the hash map and saves it to the database.
    def mapcredentials(self, username, password, data=""):
        key = hash(username)
        self.credentials.add(key, username, password, data)
        self.maptofile("credentials.txt")

    # Creates the hash map from the "credentials.txt" file
    def createmaps(self):
        self.credentials = CredentialsMap(maxsize=500, maxlistsize=10)
        self.filetomap("credentials.txt")

    # Appends to a file (fname) in current directory
    def writetofile(self, data, fname, fopen='a'):
        try:
            dir_path = os.path.dirname(os.path.realpath(__file__))
            path = dir_path + "/" + fname
            f = open(path, fopen)
            f.write(data)
            f.close()
            return True
        except Exception as e:
            print("Error writing to file " + fname + ", check logs")
            self.log(cmd="Error writing to file " + fname + ": " + str(e))
            return False

    # Writes the current hash map in memory to the file 'fname'
    def maptofile(self, fname):
        try:
            dir_path = os.path.dirname(os.path.realpath(__file__))
            path = dir_path + "/" + fname
            f = open(path, "w+")
            it = self.credentials.iterator()
            idx = 0
            while it.hasnext():
                idx += 1
                u, p, d = it.next()
                f.write(u + "|" + p + "|" + d + "\n")
            for i in range(idx, 500):
                f.write(".available|.available|.available\n")
            f.close()
            return True
        except Exception as e:
            print("Error writing to file " + fname + ", check logs")
            self.log(cmd="Error writing to file " + fname + ": " + str(e))
            return False

    # Writes the current data from 'fname' to the hash map on the server.
    def filetomap(self, fname):
        try:
            dir_path = os.path.dirname(os.path.realpath(__file__))
            path = dir_path + "/" + fname
            f = open(path, "r")
            contents = f.readlines()
            for line in contents:
                data = line.split("|")
                username = data[0]
                password = data[1]
                info = data[2]
                key = hash(username)
                if username != ".available":
                    self.credentials.add(key, username, password, info[0:len(info) - 1])
            f.close()
            return True
        except Exception as e:
            print("Error reading from file " + fname + ", check logs")
            self.log(cmd="Error reading from file " + fname + ": " + str(e))
            return False
    
    # Appends the log file for this specific day with the command that was executed.
    def log(self, cmd, a=['Server', 'Error']):
        time = self.time()
        data = time[3] + ":" + time[4] + ":" + time[5] + " <" + cmd + "> from " + str(a[0]) + ":" + str(a[1]) + "\n"
        fname = "logs/log-" + time[0] + "-" + time[1] + ".txt"
        if not self.writetofile(data,fname):
            self.console("Error logging " + cmd)

    # Used for ease of discerning debug print statements versus maintenance print statements
    def console(self, msg):
        print msg

    # Returns the time in a list format { month, day, year, hour, minute, second }
    def time(self):
        now = dt.datetime.now()
        if now.month / 10 == 0:
            month = '0' + str(now.month)
        else:
            month = str(now.month)

        if now.day / 10 == 0:
            day = '0' + str(now.day)
        else:
            day = str(now.day)

        year = str(now.year)

        if (now.hour - 5) / 10 == 0:
            hour = '0' + str(now.hour)
        else:
            hour = str(now.hour)

        if now.minute / 10 == 0:
            minute = '0' + str(now.minute)
        else:
            minute = str(now.minute)

        if now.second / 10 == 0:
            second = '0' + str(now.second)
        else:
            second = str(now.second)

        fields = [month, day, year, hour, minute, second]
        return fields

# DEVELOPER METHODS

    # Prints to Server console that client was testing connection and replies to the Client that it is receiving data properly.
    def checkconnection(self, a, c):
        print "Receiving a 'check connection' from " + str(a[0]) + ":" + str(a[1])
        c.send("debug_check_connection_received")

server = Server()
server.run()
